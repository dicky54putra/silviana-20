const flower = () => {
  const init = () => {
    onLoad();
  };

  const onLoad = () => {
    const c = setTimeout(() => {
      document.body.classList.remove("not-loaded");
      clearTimeout(c);
    }, 1000);
  };

  init();
};

export default flower;
