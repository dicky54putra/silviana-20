import flower from "./flower";

const homepage = () => {
  try {
    flower();
  } catch (e) {
    console.error(e);
  }
};

export default homepage;
